﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO.Ports;
using System.Threading;
using System.IO;

namespace Colimator_Tester
{
    public partial class Form1 : Form
    {


        volatile byte Timer_RX = 0;





        public Form1()
        {
            InitializeComponent();
        }

        private void button1_rescan_Click(object sender, EventArgs e)
        {
            try

            {
                string[] ports = SerialPort.GetPortNames();//serialPort1.GetPortNames();// Get port names

                //
                comboBox1.Items.Clear();// clear list

                comboBox1.Items.AddRange(ports);// add to list avalible ports
                comboBox1.SelectedIndex = 0;
                if (ports.Length != 0) button2_connect.Enabled = true;
                else button2_connect.Enabled = false;
                comboBox1.Enabled = true;
            }
            catch
            {
                comboBox1.Enabled = false;
                button4_send.Enabled = false;
                button2_connect.Enabled = false;
                button2_connect.Enabled = false;
                button4_send.Enabled = false;
                comboBox1.Items.Clear();// Clear list
                comboBox1.ResetText();// clear box



            }
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void button2_connect_Click(object sender, EventArgs e)
        {
            checkBox_whilesend.Enabled = true;
            button_send_data.Enabled = true;
            button3_Disconnect.Enabled = true;
            button2_connect.Enabled = false;
            button4_send.Enabled = true;
            button1_rescan.Enabled = false;
            button4_send.Enabled = true;
            //timer2.Enabled = true;
            try
            {
                serialPort1.PortName = comboBox1.Text;

                //port.PortName = ports[num];
                serialPort1.BaudRate = 115200;
                serialPort1.DataBits = 8;
                serialPort1.Parity = System.IO.Ports.Parity.None;
                serialPort1.StopBits = System.IO.Ports.StopBits.One;
                serialPort1.ReadTimeout = 1000;
                serialPort1.WriteTimeout = 1000;


                serialPort1.Open();


            }
            catch (Exception ex) { MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error); }
        }

        private void button4_send_Click(object sender, EventArgs e)
        {
            try
            {
                if (serialPort1.IsOpen) { serialPort1.WriteLine((textBox1_sender.Text)); };
            }
            catch (Exception ex) { MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error); }
        }

        private void button3_Disconnect_Click(object sender, EventArgs e)
        {
            button_send_data.Enabled = false;
            checkBox_whilesend.Checked=false;
            checkBox_whilesend.Enabled = false;
            button3_Disconnect.Enabled = false;
            button2_connect.Enabled = true;
            button4_send.Enabled = false;
            button1_rescan.Enabled = true;
            button4_send.Enabled = false;
            //timer2.Enabled = false;
            try
            {
                //serialPort1.PortName = cmbCom.Text;
                serialPort1.Close();


            }
            catch (Exception ex) { MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error); }
        }

        private void label3_Click(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            byte[] seq = { 0};
            byte[] cmd = { 0 };
            byte[] reg = { 0 };
            byte[] data = { 0 };
            byte[] data2 = { 0 };
            byte[] len = { 0 };
            byte[] cr = { 0 };
            byte  crc = 0;
            len[0]++; len[0]++;


            if (Convert.ToInt64(textBox2.Text)<255)//SEQ
            {
                seq[0] = Convert.ToByte(textBox2.Text);len[0]++;
            }
            else {
                seq[0] = 255; len[0]++;                textBox2.Clear();                textBox2.AppendText("255");
            };

           // string data2 = "010203FF";
            //byte[] i = Convert.ToSByte(data2);
            //textBox_OutData.AppendText(" ");
           // textBox_OutData.AppendText(Convert.ToString( i));
            //textBox_OutData.AppendText(" ");

            cmd[0] = Byte.Parse(textBox3.Text, System.Globalization.NumberStyles.HexNumber); len[0]++;
            //textBox_OutData.AppendText(BitConverter.ToString(cmd));



            reg[0] = Byte.Parse(textBox5.Text, System.Globalization.NumberStyles.HexNumber); len[0]++;//Convert.ToByte(textBox5.Text);
            //textBox_OutData.AppendText(BitConverter.ToString(reg));


            if (Convert.ToInt64(textBox4.Text) < 255)//data
            {
                data[0] = Convert.ToByte(textBox4.Text); len[0]++;
            }
            else
            {
                data[0] = 255; len[0]++;                textBox4.Clear();                textBox4.AppendText("255");
               // textBox_OutData.AppendText(BitConverter.ToString(data));
            };



            textBox6.Clear();// clear box data length
            textBox1.Clear();// clear box data length
            textBox1.AppendText(BitConverter.ToString(len));//add long data
            

            textBox_OutData.Clear();// 
            textBox_OutData.AppendText("C0");//
            textBox_OutData.AppendText(BitConverter.ToString(len));//len no start/end

            if (seq[0] == 0xC0) { textBox_OutData.AppendText("DBDC"); }
            else if (seq[0] == 0xDB) { textBox_OutData.AppendText("DBDD"); }
            else { textBox_OutData.AppendText(BitConverter.ToString(seq)); }//num SEQ

            textBox_OutData.AppendText(BitConverter.ToString(cmd));//out cmb
            textBox_OutData.AppendText(BitConverter.ToString(reg));//out reg


            if (data[0] == 0xC0) { textBox_OutData.AppendText("DBDC"); }
            else if (data[0] == 0xDB) { textBox_OutData.AppendText("DBDD"); }
            else
            { textBox_OutData.AppendText(BitConverter.ToString(data)); }//out data


            crc += len[0]; crc += seq[0]; crc += cmd[0]; crc += reg[0]; crc += data[0];// calc crc
            
            cr[0] = ((byte)~crc); cr[0]++;//calc crc

textBox6.AppendText(BitConverter.ToString(cr));//add crc



            if (cr[0] == 0xC0) { textBox_OutData.AppendText("DBDC"); }
            else if (cr[0] == 0xDB) { textBox_OutData.AppendText("DBDD"); }
            else
            { textBox_OutData.AppendText(BitConverter.ToString(cr)); }//print CRC



            

            textBox_OutData.AppendText("C0");//


        }

        private void radioButtoncmd00R_CheckedChanged(object sender, EventArgs e)
        {
            textBox3.Clear();
            textBox3.AppendText("00");
        }

        private void radioButtoncmd01W_CheckedChanged(object sender, EventArgs e)
        {
            textBox3.Clear();
            textBox3.AppendText("01");
        }

        private void radioButtoncmd10E_CheckedChanged(object sender, EventArgs e)
        {
            textBox3.Clear();
            textBox3.AppendText("10");
        }

        private void radioButtoncmdF0D_CheckedChanged(object sender, EventArgs e)
        {
            textBox3.Clear();
            textBox3.AppendText("F0");
        }

        private void radioButton4_CheckedChanged(object sender, EventArgs e)
        {
            textBox5.Clear();
            textBox5.AppendText("00");
        }

        private void radioButton3_CheckedChanged(object sender, EventArgs e)
        {
            textBox5.Clear();
            textBox5.AppendText("01");
        }

        private void radioButton2_CheckedChanged(object sender, EventArgs e)
        {
            textBox5.Clear();
            textBox5.AppendText("02");
        }

        private void radioButton1_CheckedChanged(object sender, EventArgs e)
        {
            textBox5.Clear();
            textBox5.AppendText("03");
        }

        private void radioButton8_CheckedChanged(object sender, EventArgs e)
        {
            textBox5.Clear();
            textBox5.AppendText("04");
        }

        private void radioButton7_CheckedChanged(object sender, EventArgs e)
        {
            textBox5.Clear();
            textBox5.AppendText("05");
        }

        private void radioButton6_CheckedChanged(object sender, EventArgs e)
        {
            textBox5.Clear();
            textBox5.AppendText("06");
        }

        private void radioButton5_CheckedChanged(object sender, EventArgs e)
        {
            textBox5.Clear();
            textBox5.AppendText("07");
        }

        private void radioButton9_CheckedChanged(object sender, EventArgs e)
        {
            textBox5.Clear();
            textBox5.AppendText("08");
        }

        private void trackBar1_Scroll(object sender, EventArgs e)
        {
           // textBox4.Clear();
            //textBox4.AppendText(trackBar1.Value.ToString());//trackBar1.Value.ToString();
            textBox4.Text = trackBar1.Value.ToString();
        }

        private void textBox3_TextChanged(object sender, EventArgs e)
        {

        }

        private void textBox3_KeyPress(object sender, KeyPressEventArgs e)
        {
            char ch = e.KeyChar;//(char)((byte)e.KeyChar-32);
            if (!((ch >= 'A' && ch <= 'F') || (ch >= 48 && ch <= 57)||(ch==10) ))
            {
               // if (ch >= 'a' && ch <= 'f') { ch = (char)((byte)e.KeyChar + 32); }


                    if (ch == 10) {
                   
                }

                    e.Handled = true;
            }
            

        }

        public static string FromHex(string hex)
        {
            hex = hex.Replace("-", "");
            byte[] raw = new byte[hex.Length / 2];
            for (int i = 0; i < raw.Length; i++)
            {
                raw[i] = Convert.ToByte(hex.Substring(i * 2, 2), 16);
            }
            return Convert.ToString( raw);//Encoding.Unicode.GetString(raw);
        }


        private void button_send_data_Click(object sender, EventArgs e)
        {
            button1.PerformClick();
            //byte[] data;
            string rawdata = textBox_OutData.Text;
            textBox_terminal.AppendText( ">> " + rawdata+ Convert.ToString((char)13)+ Convert.ToString((char)10));
            // string xmlStart = "3C 63 6F 6D 6D 6F 64 69 74 69 65 73 3E 3C 69 74 65 6D 3E 3C 69 64 3E";
            //xmlStart = xmlStart.Replace(" ", string.Empty); //clear " "
            //byte[] data = Enumerable.Range(0, xmlStart.Length / 2).Select(x => Convert.ToByte(xmlStart.Substring(x * 2, 2), 16)).ToArray();
            // string xmlStart = "3C 63 6F 6D 6D 6F 64 69 74 69 65 73 3E 3C 69 74 65 6D 3E 3C 69 64 3E";
            rawdata = rawdata.Replace(" ", string.Empty); //clear " "
            byte[] data = Enumerable.Range(0, rawdata.Length / 2).Select(x => Convert.ToByte(rawdata.Substring(x * 2, 2), 16)).ToArray();

            //byte[] data = rawdata.Split(new[] { ' ' }, StringSplitOptions.RemoveEmptyEntries).Select(i => byte.Parse(i, System.Globalization.NumberStyles.HexNumber)).ToArray();


            //string ss = 0xC0AAABACAD; ;// FromHex(rawdata); //  hex to dec
           // byte[] writestring = { 0x10, 0x03, 0x00, 0x04, 0x00, 0x04, 0x06, 0x89 };//OK!
            try
            {
                
                //serialPort1.Write(writestring, 0, writestring.Length);   //OK!
                if (serialPort1.IsOpen) { serialPort1.Write(data, 0, data.Length); };
                // if (serialPort1.IsOpen) { serialPort1.Write(); };//Convert.ToString(rawdata)

            }
            catch (Exception ex) { MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error); }


            // data = long.Parse(textBox_OutData.Text, System.Globalization.NumberStyles.HexNumber);
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            if (serialPort1.IsOpen)
            {
                if (checkBox_whilesend.Checked) { button_send_data.PerformClick(); }
            }
        }

        private void textBox4_TextChanged(object sender, EventArgs e)
        {

            
            string data = Convert.ToString(textBox4.Text);
            try
            {
                if (0 + Convert.ToInt16(data) > 0xFF)
                {

                    textBox4.Text = (Convert.ToString("255"));
                }

                trackBar1.Value = Convert.ToByte(textBox4.Text);

            }
            catch { }
        }

        private void textBox4_KeyPress(object sender, KeyPressEventArgs e)
        {
            if ((e.KeyChar >= '0') && (e.KeyChar <= '9')) { return; }
            if (Char.IsControl(e.KeyChar)) { if (e.KeyChar == (char)Keys.Back) { Text.Remove(1); }; return; }//dell symb bacspase
            e.Handled = true; //lock another Symb



        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {

        }

        private void button_Print_Screen_Click(object sender, EventArgs e)
        {
            try
            {
                Bitmap printscreen = new Bitmap(Screen.PrimaryScreen.Bounds.Width, Screen.PrimaryScreen.Bounds.Height);
                Graphics graphics = Graphics.FromImage(printscreen as Image);
                graphics.CopyFromScreen(0, 0, 0, 0, printscreen.Size);

                DateTime date1 = DateTime.Now;
                string date = date1.ToString("u");
                date = date.Replace("/", "-"); date = date.Replace(":", "-"); date = date.Replace("Z", "");
                printscreen.Save(@"C:\MicroC\Test\Emitter\ScreenShot\printscreen_" + (string)date + ".jpg", System.Drawing.Imaging.ImageFormat.Jpeg);
            }
            catch
            {
                MessageBox.Show("No access or folder does not exist: C:\\MicroC\\Test\\Emitter\\ScreenShot\\", "Save screen err", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
           // button_Connect.Enabled = true;
            // textBox_COM_RIGOL.Text = "1";

            // string path = @"C:\MicroC\Test\Emitter\EmitterMainPCB_Config_1.txt";

            char[] array2 = new char[4];

            try
            {
                StreamReader fs = new StreamReader(@"C:\MicroC\Test\Emitter\EmitterMainPCB_Config_1.txt");
                string s = fs.ReadToEnd();
                int com1 = s.IndexOf("EMITTER_COM");
                s = s.Substring(com1 + 11);
                com1 = s.IndexOf("COM");
                s = s.Substring(com1);
                string c1 = s.Substring(0, 4);//EMITTER_COM

                com1 = s.IndexOf("COLLIMATOR_COM");
                s = s.Substring(com1 + 15);
                com1 = s.IndexOf("COM");
                s = s.Substring(com1);
                string c2 = s.Substring(0, 4);//COLLIMATOR_COM

                com1 = s.IndexOf("POWER_SUPPLY_COM");
                s = s.Substring(com1 + 17);
                com1 = s.IndexOf("COM");
                s = s.Substring(com1);
                string c3 = s.Substring(0, 4);//POWER_SUPPLY_COM

                com1 = s.IndexOf("USB_RELAY_COM");
                s = s.Substring(com1 + 14);
                com1 = s.IndexOf("COM");
                s = s.Substring(com1);
                string c4 = s.Substring(0, 4);//USB_RELAY_COM


                comboBox1.Text = c1;
              //  textBox_COM_RIGOL.Text = c3;
              //  textBox_COM_RELEY.Text = c4;
              //  textBox_COM_COLLIMATOR.Text = c2;




                // string s2 = new string(array2);
                string s2 = String.Concat(array2);
                //     textBox_COM_RIGOL.AppendText(c1);
                //  textBox_terminal.AppendText(s);

            }
            catch
            {

            }
        }

        private void timer_RX_Tick(object sender, EventArgs e)
        {
            Thread.Sleep(1);
            timer_RX.Enabled = false;
            if (serialPort1.IsOpen & Timer_RX == 1)
            {
                byte[] data = new byte[serialPort1.BytesToRead + 25];
                serialPort1.Read(data, 0, data.Length - 25);
                var hexString = BitConverter.ToString(data, 0, data.Length - 25);
                hexString = hexString.Replace("-", " ");
                textBox_terminal.AppendText("<< " + hexString + Convert.ToString((char)13) + Convert.ToString((char)10));


                Timer_RX = 0;
            }
            timer_RX.Enabled = true;
        }

        private void serialPort1_DataReceived(object sender, SerialDataReceivedEventArgs e)
        {
            Timer_RX = 1;
        }
    }
}
