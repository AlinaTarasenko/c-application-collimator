﻿namespace Colimator_Tester
{
    partial class Form1
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.textBox1_sender = new System.Windows.Forms.TextBox();
            this.button4_send = new System.Windows.Forms.Button();
            this.button3_Disconnect = new System.Windows.Forms.Button();
            this.button2_connect = new System.Windows.Forms.Button();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.button1_rescan = new System.Windows.Forms.Button();
            this.serialPort1 = new System.IO.Ports.SerialPort(this.components);
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.radioButtoncmdF0D = new System.Windows.Forms.RadioButton();
            this.radioButtoncmd10E = new System.Windows.Forms.RadioButton();
            this.radioButtoncmd01W = new System.Windows.Forms.RadioButton();
            this.radioButtoncmd00R = new System.Windows.Forms.RadioButton();
            this.textBox3 = new System.Windows.Forms.TextBox();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.trackBar1 = new System.Windows.Forms.TrackBar();
            this.textBox4 = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.groupBox7 = new System.Windows.Forms.GroupBox();
            this.label6 = new System.Windows.Forms.Label();
            this.groupBox8 = new System.Windows.Forms.GroupBox();
            this.radioButton9 = new System.Windows.Forms.RadioButton();
            this.radioButton5 = new System.Windows.Forms.RadioButton();
            this.radioButton6 = new System.Windows.Forms.RadioButton();
            this.radioButton7 = new System.Windows.Forms.RadioButton();
            this.radioButton8 = new System.Windows.Forms.RadioButton();
            this.radioButton1 = new System.Windows.Forms.RadioButton();
            this.radioButton2 = new System.Windows.Forms.RadioButton();
            this.radioButton3 = new System.Windows.Forms.RadioButton();
            this.radioButton4 = new System.Windows.Forms.RadioButton();
            this.textBox5 = new System.Windows.Forms.TextBox();
            this.groupBox9 = new System.Windows.Forms.GroupBox();
            this.textBox6 = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.button_send_data = new System.Windows.Forms.Button();
            this.textBox_OutData = new System.Windows.Forms.TextBox();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.timer2 = new System.Windows.Forms.Timer(this.components);
            this.checkBox_whilesend = new System.Windows.Forms.CheckBox();
            this.button1 = new System.Windows.Forms.Button();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.textBox_terminal = new System.Windows.Forms.TextBox();
            this.button_Print_Screen = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.timer_RX = new System.Windows.Forms.Timer(this.components);
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.groupBox6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.trackBar1)).BeginInit();
            this.groupBox7.SuspendLayout();
            this.groupBox8.SuspendLayout();
            this.groupBox9.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.textBox1_sender);
            this.groupBox1.Controls.Add(this.button4_send);
            this.groupBox1.Controls.Add(this.button3_Disconnect);
            this.groupBox1.Controls.Add(this.button2_connect);
            this.groupBox1.Controls.Add(this.comboBox1);
            this.groupBox1.Controls.Add(this.button1_rescan);
            this.groupBox1.Location = new System.Drawing.Point(12, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(388, 74);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "COM Port";
            // 
            // textBox1_sender
            // 
            this.textBox1_sender.Location = new System.Drawing.Point(6, 48);
            this.textBox1_sender.Name = "textBox1_sender";
            this.textBox1_sender.Size = new System.Drawing.Size(285, 20);
            this.textBox1_sender.TabIndex = 5;
            // 
            // button4_send
            // 
            this.button4_send.Enabled = false;
            this.button4_send.Location = new System.Drawing.Point(297, 46);
            this.button4_send.Name = "button4_send";
            this.button4_send.Size = new System.Drawing.Size(75, 23);
            this.button4_send.TabIndex = 4;
            this.button4_send.Text = "Send";
            this.button4_send.UseVisualStyleBackColor = true;
            this.button4_send.Click += new System.EventHandler(this.button4_send_Click);
            // 
            // button3_Disconnect
            // 
            this.button3_Disconnect.Enabled = false;
            this.button3_Disconnect.Location = new System.Drawing.Point(297, 18);
            this.button3_Disconnect.Name = "button3_Disconnect";
            this.button3_Disconnect.Size = new System.Drawing.Size(75, 23);
            this.button3_Disconnect.TabIndex = 3;
            this.button3_Disconnect.Text = "Disconnect";
            this.button3_Disconnect.UseVisualStyleBackColor = true;
            this.button3_Disconnect.Click += new System.EventHandler(this.button3_Disconnect_Click);
            // 
            // button2_connect
            // 
            this.button2_connect.Enabled = false;
            this.button2_connect.Location = new System.Drawing.Point(216, 18);
            this.button2_connect.Name = "button2_connect";
            this.button2_connect.Size = new System.Drawing.Size(75, 23);
            this.button2_connect.TabIndex = 2;
            this.button2_connect.Text = "Connect";
            this.button2_connect.UseVisualStyleBackColor = true;
            this.button2_connect.Click += new System.EventHandler(this.button2_connect_Click);
            // 
            // comboBox1
            // 
            this.comboBox1.Enabled = false;
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Location = new System.Drawing.Point(88, 20);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(121, 21);
            this.comboBox1.TabIndex = 1;
            this.comboBox1.SelectedIndexChanged += new System.EventHandler(this.comboBox1_SelectedIndexChanged);
            // 
            // button1_rescan
            // 
            this.button1_rescan.Location = new System.Drawing.Point(6, 19);
            this.button1_rescan.Name = "button1_rescan";
            this.button1_rescan.Size = new System.Drawing.Size(75, 23);
            this.button1_rescan.TabIndex = 0;
            this.button1_rescan.Text = "Rescan";
            this.button1_rescan.UseVisualStyleBackColor = true;
            this.button1_rescan.Click += new System.EventHandler(this.button1_rescan_Click);
            // 
            // serialPort1
            // 
            this.serialPort1.BaudRate = 115200;
            this.serialPort1.DataReceived += new System.IO.Ports.SerialDataReceivedEventHandler(this.serialPort1_DataReceived);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.label1);
            this.groupBox2.Location = new System.Drawing.Point(12, 92);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(47, 252);
            this.groupBox2.TabIndex = 1;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Start";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 16);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(31, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "0xC0";
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.textBox1);
            this.groupBox3.Controls.Add(this.label2);
            this.groupBox3.Location = new System.Drawing.Point(65, 92);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(66, 252);
            this.groupBox3.TabIndex = 2;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Length";
            // 
            // textBox1
            // 
            this.textBox1.BackColor = System.Drawing.SystemColors.Menu;
            this.textBox1.Location = new System.Drawing.Point(6, 19);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(53, 20);
            this.textBox1.TabIndex = 1;
            this.textBox1.Text = "0";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(7, 42);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(53, 39);
            this.label2.TabIndex = 0;
            this.label2.Text = "0-10\r\ncalc\r\nautomatic";
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.textBox2);
            this.groupBox4.Controls.Add(this.label3);
            this.groupBox4.Location = new System.Drawing.Point(137, 92);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(71, 252);
            this.groupBox4.TabIndex = 3;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Sequence";
            // 
            // textBox2
            // 
            this.textBox2.Location = new System.Drawing.Point(7, 19);
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new System.Drawing.Size(53, 20);
            this.textBox2.TabIndex = 1;
            this.textBox2.Text = "0";
            this.textBox2.TextChanged += new System.EventHandler(this.textBox2_TextChanged);
            this.textBox2.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBox4_KeyPress);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(16, 42);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(34, 13);
            this.label3.TabIndex = 0;
            this.label3.Text = "0-255";
            this.label3.Click += new System.EventHandler(this.label3_Click);
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.radioButtoncmdF0D);
            this.groupBox5.Controls.Add(this.radioButtoncmd10E);
            this.groupBox5.Controls.Add(this.radioButtoncmd01W);
            this.groupBox5.Controls.Add(this.radioButtoncmd00R);
            this.groupBox5.Controls.Add(this.textBox3);
            this.groupBox5.Location = new System.Drawing.Point(214, 92);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(99, 252);
            this.groupBox5.TabIndex = 3;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "Command";
            // 
            // radioButtoncmdF0D
            // 
            this.radioButtoncmdF0D.AutoSize = true;
            this.radioButtoncmdF0D.Location = new System.Drawing.Point(6, 114);
            this.radioButtoncmdF0D.Name = "radioButtoncmdF0D";
            this.radioButtoncmdF0D.Size = new System.Drawing.Size(83, 17);
            this.radioButtoncmdF0D.TabIndex = 5;
            this.radioButtoncmdF0D.Text = "0xF0 Debug";
            this.radioButtoncmdF0D.UseVisualStyleBackColor = true;
            this.radioButtoncmdF0D.CheckedChanged += new System.EventHandler(this.radioButtoncmdF0D_CheckedChanged);
            // 
            // radioButtoncmd10E
            // 
            this.radioButtoncmd10E.AutoSize = true;
            this.radioButtoncmd10E.Location = new System.Drawing.Point(6, 91);
            this.radioButtoncmd10E.Name = "radioButtoncmd10E";
            this.radioButtoncmd10E.Size = new System.Drawing.Size(73, 17);
            this.radioButtoncmd10E.TabIndex = 4;
            this.radioButtoncmd10E.Text = "0x10 Error";
            this.radioButtoncmd10E.UseVisualStyleBackColor = true;
            this.radioButtoncmd10E.CheckedChanged += new System.EventHandler(this.radioButtoncmd10E_CheckedChanged);
            // 
            // radioButtoncmd01W
            // 
            this.radioButtoncmd01W.AutoSize = true;
            this.radioButtoncmd01W.Location = new System.Drawing.Point(6, 68);
            this.radioButtoncmd01W.Name = "radioButtoncmd01W";
            this.radioButtoncmd01W.Size = new System.Drawing.Size(76, 17);
            this.radioButtoncmd01W.TabIndex = 3;
            this.radioButtoncmd01W.Text = "0x01 Write";
            this.radioButtoncmd01W.UseVisualStyleBackColor = true;
            this.radioButtoncmd01W.CheckedChanged += new System.EventHandler(this.radioButtoncmd01W_CheckedChanged);
            // 
            // radioButtoncmd00R
            // 
            this.radioButtoncmd00R.AutoSize = true;
            this.radioButtoncmd00R.Checked = true;
            this.radioButtoncmd00R.Location = new System.Drawing.Point(6, 45);
            this.radioButtoncmd00R.Name = "radioButtoncmd00R";
            this.radioButtoncmd00R.Size = new System.Drawing.Size(77, 17);
            this.radioButtoncmd00R.TabIndex = 2;
            this.radioButtoncmd00R.TabStop = true;
            this.radioButtoncmd00R.Text = "0x00 Read";
            this.radioButtoncmd00R.UseVisualStyleBackColor = true;
            this.radioButtoncmd00R.CheckedChanged += new System.EventHandler(this.radioButtoncmd00R_CheckedChanged);
            // 
            // textBox3
            // 
            this.textBox3.Location = new System.Drawing.Point(6, 19);
            this.textBox3.Name = "textBox3";
            this.textBox3.Size = new System.Drawing.Size(83, 20);
            this.textBox3.TabIndex = 1;
            this.textBox3.Text = "00";
            this.textBox3.TextChanged += new System.EventHandler(this.textBox3_TextChanged);
            this.textBox3.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBox3_KeyPress);
            // 
            // groupBox6
            // 
            this.groupBox6.Controls.Add(this.trackBar1);
            this.groupBox6.Controls.Add(this.textBox4);
            this.groupBox6.Controls.Add(this.label5);
            this.groupBox6.Location = new System.Drawing.Point(493, 92);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Size = new System.Drawing.Size(115, 252);
            this.groupBox6.TabIndex = 4;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = "Data";
            // 
            // trackBar1
            // 
            this.trackBar1.Location = new System.Drawing.Point(5, 68);
            this.trackBar1.Maximum = 255;
            this.trackBar1.Name = "trackBar1";
            this.trackBar1.Size = new System.Drawing.Size(104, 45);
            this.trackBar1.TabIndex = 20;
            this.trackBar1.Scroll += new System.EventHandler(this.trackBar1_Scroll);
            // 
            // textBox4
            // 
            this.textBox4.Location = new System.Drawing.Point(6, 19);
            this.textBox4.Name = "textBox4";
            this.textBox4.Size = new System.Drawing.Size(103, 20);
            this.textBox4.TabIndex = 1;
            this.textBox4.Text = "0";
            this.textBox4.TextChanged += new System.EventHandler(this.textBox4_TextChanged);
            this.textBox4.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBox4_KeyPress);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(16, 45);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(34, 13);
            this.label5.TabIndex = 0;
            this.label5.Text = "0-255";
            // 
            // groupBox7
            // 
            this.groupBox7.Controls.Add(this.label6);
            this.groupBox7.Location = new System.Drawing.Point(686, 92);
            this.groupBox7.Name = "groupBox7";
            this.groupBox7.Size = new System.Drawing.Size(47, 252);
            this.groupBox7.TabIndex = 2;
            this.groupBox7.TabStop = false;
            this.groupBox7.Text = "End";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(6, 16);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(31, 13);
            this.label6.TabIndex = 0;
            this.label6.Text = "0xC0";
            // 
            // groupBox8
            // 
            this.groupBox8.Controls.Add(this.radioButton9);
            this.groupBox8.Controls.Add(this.radioButton5);
            this.groupBox8.Controls.Add(this.radioButton6);
            this.groupBox8.Controls.Add(this.radioButton7);
            this.groupBox8.Controls.Add(this.radioButton8);
            this.groupBox8.Controls.Add(this.radioButton1);
            this.groupBox8.Controls.Add(this.radioButton2);
            this.groupBox8.Controls.Add(this.radioButton3);
            this.groupBox8.Controls.Add(this.radioButton4);
            this.groupBox8.Controls.Add(this.textBox5);
            this.groupBox8.Location = new System.Drawing.Point(319, 92);
            this.groupBox8.Name = "groupBox8";
            this.groupBox8.Size = new System.Drawing.Size(168, 252);
            this.groupBox8.TabIndex = 6;
            this.groupBox8.TabStop = false;
            this.groupBox8.Text = "Register";
            // 
            // radioButton9
            // 
            this.radioButton9.AutoSize = true;
            this.radioButton9.Location = new System.Drawing.Point(6, 229);
            this.radioButton9.Name = "radioButton9";
            this.radioButton9.Size = new System.Drawing.Size(144, 17);
            this.radioButton9.TabIndex = 10;
            this.radioButton9.Text = "0x08 HeartBeat In (R/W)";
            this.radioButton9.UseVisualStyleBackColor = true;
            this.radioButton9.CheckedChanged += new System.EventHandler(this.radioButton9_CheckedChanged);
            // 
            // radioButton5
            // 
            this.radioButton5.AutoSize = true;
            this.radioButton5.Location = new System.Drawing.Point(6, 206);
            this.radioButton5.Name = "radioButton5";
            this.radioButton5.Size = new System.Drawing.Size(152, 17);
            this.radioButton5.TabIndex = 9;
            this.radioButton5.Text = "0x07 HeartBeat Out (R/W)";
            this.radioButton5.UseVisualStyleBackColor = true;
            this.radioButton5.CheckedChanged += new System.EventHandler(this.radioButton5_CheckedChanged);
            // 
            // radioButton6
            // 
            this.radioButton6.AutoSize = true;
            this.radioButton6.Location = new System.Drawing.Point(6, 183);
            this.radioButton6.Name = "radioButton6";
            this.radioButton6.Size = new System.Drawing.Size(100, 17);
            this.radioButton6.TabIndex = 8;
            this.radioButton6.Text = "0x06 Calibration";
            this.radioButton6.UseVisualStyleBackColor = true;
            this.radioButton6.CheckedChanged += new System.EventHandler(this.radioButton6_CheckedChanged);
            // 
            // radioButton7
            // 
            this.radioButton7.AutoSize = true;
            this.radioButton7.Location = new System.Drawing.Point(6, 160);
            this.radioButton7.Name = "radioButton7";
            this.radioButton7.Size = new System.Drawing.Size(160, 17);
            this.radioButton7.TabIndex = 7;
            this.radioButton7.Text = "0x05 Motor 2 Position (R/W)";
            this.radioButton7.UseVisualStyleBackColor = true;
            this.radioButton7.CheckedChanged += new System.EventHandler(this.radioButton7_CheckedChanged);
            // 
            // radioButton8
            // 
            this.radioButton8.AutoSize = true;
            this.radioButton8.Location = new System.Drawing.Point(6, 137);
            this.radioButton8.Name = "radioButton8";
            this.radioButton8.Size = new System.Drawing.Size(160, 17);
            this.radioButton8.TabIndex = 6;
            this.radioButton8.Text = "0x04 Motor 1 Position (R/W)";
            this.radioButton8.UseVisualStyleBackColor = true;
            this.radioButton8.CheckedChanged += new System.EventHandler(this.radioButton8_CheckedChanged);
            // 
            // radioButton1
            // 
            this.radioButton1.AutoSize = true;
            this.radioButton1.Location = new System.Drawing.Point(6, 114);
            this.radioButton1.Name = "radioButton1";
            this.radioButton1.Size = new System.Drawing.Size(123, 17);
            this.radioButton1.TabIndex = 5;
            this.radioButton1.Text = "0x03 FW Version (R)";
            this.radioButton1.UseVisualStyleBackColor = true;
            this.radioButton1.CheckedChanged += new System.EventHandler(this.radioButton1_CheckedChanged);
            // 
            // radioButton2
            // 
            this.radioButton2.AutoSize = true;
            this.radioButton2.Location = new System.Drawing.Point(6, 91);
            this.radioButton2.Name = "radioButton2";
            this.radioButton2.Size = new System.Drawing.Size(119, 17);
            this.radioButton2.TabIndex = 4;
            this.radioButton2.Text = "0x02 Event Flag (R)";
            this.radioButton2.UseVisualStyleBackColor = true;
            this.radioButton2.CheckedChanged += new System.EventHandler(this.radioButton2_CheckedChanged);
            // 
            // radioButton3
            // 
            this.radioButton3.AutoSize = true;
            this.radioButton3.Location = new System.Drawing.Point(6, 68);
            this.radioButton3.Name = "radioButton3";
            this.radioButton3.Size = new System.Drawing.Size(141, 17);
            this.radioButton3.TabIndex = 3;
            this.radioButton3.Text = "0x01 Event Mask (R/W)";
            this.radioButton3.UseVisualStyleBackColor = true;
            this.radioButton3.CheckedChanged += new System.EventHandler(this.radioButton3_CheckedChanged);
            // 
            // radioButton4
            // 
            this.radioButton4.AutoSize = true;
            this.radioButton4.Checked = true;
            this.radioButton4.Location = new System.Drawing.Point(6, 45);
            this.radioButton4.Name = "radioButton4";
            this.radioButton4.Size = new System.Drawing.Size(125, 17);
            this.radioButton4.TabIndex = 2;
            this.radioButton4.TabStop = true;
            this.radioButton4.Text = "0x00 Device type (R)";
            this.radioButton4.UseVisualStyleBackColor = true;
            this.radioButton4.CheckedChanged += new System.EventHandler(this.radioButton4_CheckedChanged);
            // 
            // textBox5
            // 
            this.textBox5.Location = new System.Drawing.Point(6, 19);
            this.textBox5.Name = "textBox5";
            this.textBox5.Size = new System.Drawing.Size(83, 20);
            this.textBox5.TabIndex = 1;
            this.textBox5.Text = "00";
            // 
            // groupBox9
            // 
            this.groupBox9.Controls.Add(this.textBox6);
            this.groupBox9.Controls.Add(this.label4);
            this.groupBox9.Location = new System.Drawing.Point(614, 92);
            this.groupBox9.Name = "groupBox9";
            this.groupBox9.Size = new System.Drawing.Size(66, 252);
            this.groupBox9.TabIndex = 5;
            this.groupBox9.TabStop = false;
            this.groupBox9.Text = "CRC";
            // 
            // textBox6
            // 
            this.textBox6.BackColor = System.Drawing.SystemColors.MenuBar;
            this.textBox6.Location = new System.Drawing.Point(6, 19);
            this.textBox6.Name = "textBox6";
            this.textBox6.Size = new System.Drawing.Size(53, 20);
            this.textBox6.TabIndex = 1;
            this.textBox6.Text = "0";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(7, 42);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(53, 39);
            this.label4.TabIndex = 0;
            this.label4.Text = "~Sum+1;\r\ncalc\r\nautomatic";
            // 
            // button_send_data
            // 
            this.button_send_data.Enabled = false;
            this.button_send_data.Location = new System.Drawing.Point(324, 348);
            this.button_send_data.Name = "button_send_data";
            this.button_send_data.Size = new System.Drawing.Size(162, 23);
            this.button_send_data.TabIndex = 7;
            this.button_send_data.Text = "Send data";
            this.button_send_data.UseVisualStyleBackColor = true;
            this.button_send_data.Click += new System.EventHandler(this.button_send_data_Click);
            // 
            // textBox_OutData
            // 
            this.textBox_OutData.Location = new System.Drawing.Point(99, 351);
            this.textBox_OutData.Name = "textBox_OutData";
            this.textBox_OutData.Size = new System.Drawing.Size(219, 20);
            this.textBox_OutData.TabIndex = 8;
            // 
            // timer1
            // 
            this.timer1.Enabled = true;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // checkBox_whilesend
            // 
            this.checkBox_whilesend.AutoSize = true;
            this.checkBox_whilesend.Enabled = false;
            this.checkBox_whilesend.Location = new System.Drawing.Point(622, 354);
            this.checkBox_whilesend.Name = "checkBox_whilesend";
            this.checkBox_whilesend.Size = new System.Drawing.Size(104, 17);
            this.checkBox_whilesend.TabIndex = 9;
            this.checkBox_whilesend.Text = "Auto Send 10Hz";
            this.checkBox_whilesend.UseVisualStyleBackColor = true;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(12, 350);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 10;
            this.button1.Text = "Calc data";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label7.Location = new System.Drawing.Point(406, 20);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(205, 31);
            this.label7.TabIndex = 11;
            this.label7.Text = "Micro C System";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label8.Location = new System.Drawing.Point(409, 60);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(303, 24);
            this.label8.TabIndex = 12;
            this.label8.Text = "Emitter Collimator Transmitter v0.01";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(509, 355);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(72, 13);
            this.label9.TabIndex = 13;
            this.label9.Text = "115200 8-N-1";
            // 
            // textBox_terminal
            // 
            this.textBox_terminal.Location = new System.Drawing.Point(12, 380);
            this.textBox_terminal.Multiline = true;
            this.textBox_terminal.Name = "textBox_terminal";
            this.textBox_terminal.Size = new System.Drawing.Size(721, 226);
            this.textBox_terminal.TabIndex = 14;
            // 
            // button_Print_Screen
            // 
            this.button_Print_Screen.Location = new System.Drawing.Point(614, 12);
            this.button_Print_Screen.Name = "button_Print_Screen";
            this.button_Print_Screen.Size = new System.Drawing.Size(119, 23);
            this.button_Print_Screen.TabIndex = 72;
            this.button_Print_Screen.Text = "Print Screen";
            this.button_Print_Screen.UseVisualStyleBackColor = true;
            this.button_Print_Screen.Click += new System.EventHandler(this.button_Print_Screen_Click);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(649, 37);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(63, 20);
            this.button2.TabIndex = 21;
            this.button2.Text = "Read txt";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // timer_RX
            // 
            this.timer_RX.Enabled = true;
            this.timer_RX.Tick += new System.EventHandler(this.timer_RX_Tick);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(739, 618);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button_Print_Screen);
            this.Controls.Add(this.textBox_terminal);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.checkBox_whilesend);
            this.Controls.Add(this.textBox_OutData);
            this.Controls.Add(this.button_send_data);
            this.Controls.Add(this.groupBox9);
            this.Controls.Add(this.groupBox8);
            this.Controls.Add(this.groupBox7);
            this.Controls.Add(this.groupBox6);
            this.Controls.Add(this.groupBox5);
            this.Controls.Add(this.groupBox4);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Name = "Form1";
            this.Text = "Colimator Tester";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            this.groupBox6.ResumeLayout(false);
            this.groupBox6.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.trackBar1)).EndInit();
            this.groupBox7.ResumeLayout(false);
            this.groupBox7.PerformLayout();
            this.groupBox8.ResumeLayout(false);
            this.groupBox8.PerformLayout();
            this.groupBox9.ResumeLayout(false);
            this.groupBox9.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button button4_send;
        private System.Windows.Forms.Button button3_Disconnect;
        private System.Windows.Forms.Button button2_connect;
        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.Button button1_rescan;
        private System.IO.Ports.SerialPort serialPort1;
        private System.Windows.Forms.TextBox textBox1_sender;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.RadioButton radioButtoncmdF0D;
        private System.Windows.Forms.RadioButton radioButtoncmd10E;
        private System.Windows.Forms.RadioButton radioButtoncmd01W;
        private System.Windows.Forms.RadioButton radioButtoncmd00R;
        private System.Windows.Forms.TextBox textBox3;
        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.TextBox textBox4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.GroupBox groupBox7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.GroupBox groupBox8;
        private System.Windows.Forms.RadioButton radioButton1;
        private System.Windows.Forms.RadioButton radioButton2;
        private System.Windows.Forms.RadioButton radioButton3;
        private System.Windows.Forms.RadioButton radioButton4;
        private System.Windows.Forms.TextBox textBox5;
        private System.Windows.Forms.TrackBar trackBar1;
        private System.Windows.Forms.RadioButton radioButton9;
        private System.Windows.Forms.RadioButton radioButton5;
        private System.Windows.Forms.RadioButton radioButton6;
        private System.Windows.Forms.RadioButton radioButton7;
        private System.Windows.Forms.RadioButton radioButton8;
        private System.Windows.Forms.GroupBox groupBox9;
        private System.Windows.Forms.TextBox textBox6;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button button_send_data;
        private System.Windows.Forms.TextBox textBox_OutData;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.Timer timer2;
        private System.Windows.Forms.CheckBox checkBox_whilesend;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox textBox_terminal;
        private System.Windows.Forms.Button button_Print_Screen;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Timer timer_RX;
    }
}

